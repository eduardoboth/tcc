with open("qualis.txt", encoding = "utf8") as file:
    lines = file.readlines()

tokens = []

with open("qualisJson.txt", "a+") as outFile:
    for line in lines:
        tokens = line.split('"')
        jsonLine = '"' + tokens[1].replace('-', '', 1).strip() + '": { "titulo": "' +  tokens[3].strip() + '", "areaDeAvaliacao": "' + tokens[5].strip() + '", "estrato": "' + tokens[7].strip() + '" },\n'
        outFile.write(jsonLine)