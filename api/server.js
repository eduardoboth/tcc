var express = require('express');
var restResponse = require('express-rest-response');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var xmlBodyParser = require('express-xml-bodyparser');

var server = express();
server.use(logger('dev'));
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(xmlBodyParser());

var options = {
	showStatusCode: false,
	showDefaultMessage: false
};
server.use(restResponse(options));

server.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

server.use('/', require('./routes'));

const port = process.env.PORT || 8050;
server.listen(port);
console.log('Server started on port ' + port);

module.exports = server;