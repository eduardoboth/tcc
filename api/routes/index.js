var express = require('express');
var router = express.Router();
 
var publico = require('./publico.js');
var admin = require('./admin.js');
 
router.get('/api/v1/autores', publico.getAllAutores);
router.get('/api/v1/autores/:id', publico.getOneAutor);

router.get('/api/v1/publicacoes', publico.getAllPublicacoes);
router.get('/api/v1/publicacoes/:id', publico.getOnePublicacao);

router.get('/api/v1/grupos', publico.getAllGrupos);
router.get('/api/v1/grupos/:id', publico.getOneGrupo);

router.post('/api/v1/indices', publico.getIndicesGrupoAnonimo);
router.get('/api/v1/indices/:id', publico.getIndicesGrupo);
 
// Admin
router.post('/api/v1/admin/grupos/', admin.createGrupo);

router.post('/api/v1/admin/uploadCurriculo/', admin.uploadCurriculo);
router.post('/api/v1/admin/parseCurriculo/', admin.parseCurriculo);
 
module.exports = router;