var fs = require('fs');
var shortid = require('shortid');
var neo4j = require('neo4j-driver').v1;

var appConfig = JSON.parse(fs.readFileSync('./config/appConfig.json', 'utf8'));
var driver = neo4j.driver(appConfig.neo4jURL, neo4j.auth.basic(appConfig.neo4jUser, appConfig.neo4jPwd));

var pesosQualis = JSON.parse(fs.readFileSync('./config/pesosQualis.json', 'utf8'));
var pesosTipoPub = JSON.parse(fs.readFileSync('./config/pesosTipoPublicacao.json', 'utf8'));

var publico = {

    getAllAutores: function (req, res) {
        var session = driver.session();
        session
            .run("MATCH (a:Autor) WHERE a.curriculoCompleto = true RETURN a ORDER BY a.nomeCompleto ASC")
            .then(function (result) {
                session.close();

                var autores = {};
                autores.autores = [];
                result.records.forEach(function (record) {
                    autores.autores.push(record._fields[0].properties);
                });

                res.json(autores);
            })
            .catch(function (err) {
                session.close();
                res.rest.serverError(err);
            });
    },

    getOneAutor: function (req, res) {
        var id = req.params.id;

        var autor;
        var session = driver.session();
        session
            .run("MATCH (a:Autor) WHERE a.numeroIdCNPQ = {id} RETURN a", { id: id })
            .then(function (result) {
                autor = result.records[0]._fields[0].properties;

                var queryIndices =
                    "MATCH (p:Publicacao)<-[rel:CONTRIBUIU]-(a:Autor) WHERE a.numeroIdCNPQ = {id} " +
                    "WITH { tipo: p.tipo, qualis: p.qualis, nAutoresNoGrupo: 1, anoTrabalho: p.anoDoTrabalho, anoArtigo: p.anoDoArtigo } AS metaPublicacao " +
                    "RETURN metaPublicacao";
                session
                    .run(queryIndices, { id: id })
                    .then(function (result) {
                        autor.indices = calculaIndices(result.records);

                        var queryPublicacoes = "MATCH (p:Publicacao)<-[:CONTRIBUIU]-(a:Autor) WHERE a.numeroIdCNPQ = {id}" +
                            "WITH p " +
                            "MATCH (pp:Publicacao)<-[:CONTRIBUIU]-(aa:Autor) WHERE p = pp and aa.numeroIdCNPQ <> {id} " +
                            "WITH p, collect(aa) as autores " +
                            "RETURN p, autores";

                        session.run(queryPublicacoes, { id: id })
                            .then(function (result) {
                                var trabalhosEmEvento = result.records.filter(filtroTipoTrabalhoEmEvento).sort(function (a, b) { return b._fields[0].properties.anoDoTrabalho - a._fields[0].properties.anoDoTrabalho; });

                                autor.publicacoes = {};

                                autor.publicacoes.trabalhosEmEvento = [];

                                trabalhosEmEvento.forEach(function (record) {
                                    var autores = [];

                                    record._fields[1].forEach(function (recordAutor) {
                                        autores.push(recordAutor.properties);
                                    });

                                    record._fields[0].properties.autores = autores;

                                    autor.publicacoes.trabalhosEmEvento.push(record._fields[0].properties);
                                });

                                var artigosPublicados = result.records.filter(filtroTipoArtigoPublicado).sort(function (a, b) { return b._fields[0].properties.anoDoArtigo - a._fields[0].properties.anoDoArtigo; });;

                                autor.publicacoes.artigosPublicados = [];

                                artigosPublicados.forEach(function (record) {
                                    var autores = [];

                                    record._fields[1].forEach(function (recordAutor) {
                                        autores.push(recordAutor.properties);
                                    });

                                    record._fields[0].properties.autores = autores;

                                    autor.publicacoes.artigosPublicados.push(record._fields[0].properties);
                                });

                                session.run("MATCH (a:Autor)-[:CONTRIBUIU]->(p:Publicacao)<-[:CONTRIBUIU]-(b:Autor) WHERE a.numeroIdCNPQ = {id} WITH b ORDER BY b.nomeCompleto RETURN COLLECT(DISTINCT b.nomeCompleto)", { id: id })
                                    .then(function (result) {
                                        autor.colaboracoes = result.records[0]._fields[0];

                                        session.close();
                                        res.json(autor);
                                    })
                                    .catch(function (err) {
                                        session.close();
                                        res.rest.serverError(err);
                                    });
                            })
                            .catch(function (err) {
                                session.close();
                                res.rest.serverError(err);
                            });
                    })
                    .catch(function (err) {
                        session.close();
                        res.rest.serverError(err);
                    });
            })
            .catch(function (err) {
                session.close();
                res.rest.serverError(err);
            });
    },

    getAllPublicacoes: function (req, res) {
        var session = driver.session();
        session
            .run("MATCH (p:Publicacao)<-[:CONTRIBUIU]-(a:Autor) " +
            "WITH p, collect(a.numeroIdCNPQ) as autores " +
            "RETURN p, autores")
            .then(function (result) {
                session.close();

                var publicacoes = JSON.parse('{ "publicacoes": [] }');
                result.records.forEach(function (record) {
                    record._fields[0].properties.autores = record._fields[1];
                    publicacoes.publicacoes.push(record._fields[0].properties);
                });

                res.json(publicacoes);
            })
            .catch(function (err) {
                session.close();
                res.rest.serverError(err);
            });
    },

    getOnePublicacao: function (req, res) {
        var id = req.params.id;
        var session = driver.session();
        session
            .run("MATCH (p:Publicacao)<-[:CONTRIBUIU]-(a:Autor) WHERE p.uuid = {id} " +
            "WITH p, collect(a.numeroIdCNPQ) as autores " +
            "RETURN p, autores", { id: id })
            .then(function (result) {
                session.close();
                result.records[0]._fields[0].properties.autores = result.records[0]._fields[1];
                res.json(result.records[0]._fields[0].properties);
            })
            .catch(function (err) {
                session.close();
                res.rest.serverError(err);
            });
    },

    getAllGrupos: function (req, res) {
        var session = driver.session();
        session
            .run("MATCH (g:Grupo)<-[:MEMBRO]-(a:Autor) " +
            "WITH g, collect(a.numeroIdCNPQ) as autores " +
            "RETURN g, autores")
            .then(function (result) {
                session.close();

                var grupos = JSON.parse('{ "grupos": [] }');
                result.records.forEach(function (record) {
                    record._fields[0].properties.membros = record._fields[1];
                    grupos.grupos.push(record._fields[0].properties);
                });

                res.json(grupos);
            })
            .catch(function (err) {
                session.close();
                res.rest.serverError(err);
            });
    },

    getOneGrupo: function (req, res) {
        var id = req.params.id;
        var session = driver.session();
        session
            .run("MATCH (g:Grupo)<-[:MEMBRO]-(a:Autor) WHERE g.uuid = {id} " +
            "WITH g, collect(distinct a.numeroIdCNPQ) as autores " +
            "RETURN g, autores", { id: id })
            .then(function (result) {
                session.close();
                result.records[0]._fields[0].properties.membros = result.records[0]._fields[1];
                res.json(result.records[0]._fields[0].properties);
            })
            .catch(function (err) {
                session.close();
                res.rest.serverError(err);
            });
    },

    getIndicesGrupoAnonimo: function (req, res) {
        var grupo = req.body;

        // Pontuação somente leva em conta grupo fornecido, não outros grupos
        var query =
            "UNWIND {grupo} AS idAutor " +
            "MATCH (p:Publicacao)<-[:CONTRIBUIU]-(a:Autor) WHERE a.numeroIdCNPQ = idAutor " +
            "WITH p " +
            "MATCH (pp:Publicacao)<-[rel:CONTRIBUIU]-(ca:Autor) WHERE pp.uuid = p.uuid AND ca.numeroIdCNPQ IN {grupo} " +
            "WITH p, { tipo: p.tipo, qualis: p.qualis, nAutoresNoGrupo: count(rel), anoTrabalho: p.anoDoTrabalho, anoArtigo: p.anoDoArtigo } AS metaPublicacao " +
            "RETURN metaPublicacao";

        var session = driver.session();
        session
            .run(query, { grupo: grupo.membros })
            .then(function (result) {
                session.close();
                res.json(calculaIndices(result.records));
            })
            .catch(function (err) {
                session.close();
                res.rest.serverError(err);
            });

    },

    getIndicesGrupo: function (req, res) {
        var id = req.params.id;
        var grupo = req.body;

        // Pontuação somente leva em conta grupo fornecido, não outros grupos
        var query =
            "UNWIND {grupo} AS idAutor " +
            "MATCH (p:Publicacao)<-[:CONTRIBUIU]-(a:Autor) WHERE a.numeroIdCNPQ = idAutor " +
            "WITH p " +
            "MATCH (pp:Publicacao)<-[rel:CONTRIBUIU]-(ca:Autor) WHERE pp.uuid = p.uuid AND ca.numeroIdCNPQ IN {grupo} " +
            "WITH p, { tipo: p.tipo, qualis: p.qualis, nAutoresNoGrupo: count(rel), anoTrabalho: p.anoDoTrabalho, anoArtigo: p.anoDoArtigo } AS metaPublicacao " +
            "RETURN metaPublicacao";

        var session = driver.session();
        session
            .run("MATCH (g:Grupo)<-[:MEMBRO]-(a:Autor) WHERE g.uuid = {id} " +
            "WITH { membros: collect(a.numeroIdCNPQ) } AS autores " +
            "RETURN autores", { id: id })
            .then(function (grupo) {
                session
                    .run(query, { grupo: grupo.records[0]._fields[0].membros })
                    .then(function (result) {
                        session.close();
                        res.json(calculaIndices(result.records));
                    })
                    .catch(function (err) {
                        session.close();
                        res.rest.serverError(err);
                    })
            })
            .catch(function (err) {
                session.close();
                res.rest.serverError(err);
            });
    }
};

module.exports = publico;

function calculaIndices(registros) {
    var indices = JSON.parse('{ ' +
        '"indiceGeralTrabalhoEmEvento": 0, ' +
        '"indiceGeralArtigoPublicado": 0, ' +
        '"indiceGeral": 0, ' +
        '"indiceRestritoTrabalhoEmEvento": 0, ' +
        '"indiceRestritoArtigoPublicado": 0, ' +
        '"indiceRestrito": 0 ' +
        ' }');

    var trabalhosEmEvento = [];
    var trabalhosEmEventoRestrito = [];

    var contArtigosPublicados = 0;
    var contArtigosPublicadosRestrito = 0;

    registros.forEach(function (registro) {
        var anoPub;
        if (registro._fields[0].tipo === 'trabalhoEmEvento') {
            anoPub = parseInt(registro._fields[0].anoTrabalho, 10);
        }

        if (registro._fields[0].tipo === 'artigoPublicado') {
            anoPub = parseInt(registro._fields[0].anoArtigo, 10);
        }

        var anoAtual = new Date().getFullYear();
        if (anoPub > (anoAtual - 4)) {

            var indice = pesosTipoPub[registro._fields[0].tipo] * pesosQualis[registro._fields[0].qualis] / registro._fields[0].nAutoresNoGrupo.toInt();

            if (registro._fields[0].tipo === 'trabalhoEmEvento') {
                trabalhosEmEvento.push(indice);
            }

            if (registro._fields[0].tipo === 'artigoPublicado') {
                indices.indiceGeralArtigoPublicado += indice;
                contArtigosPublicados++;
            }

            if (pesosQualis[registro._fields[0].qualis] >= pesosQualis['B1']) {
                if (registro._fields[0].tipo === 'trabalhoEmEvento') {
                    trabalhosEmEventoRestrito.push(indice);
                }

                if (registro._fields[0].tipo === 'artigoPublicado') {
                    indices.indiceRestritoArtigoPublicado += indice;
                    contArtigosPublicadosRestrito++;
                }
            }
        }
    });

    // Restrição: No máximo 3 conferências a cada 1 periodico são contabilizadas - priorizando as de maior valor.
    trabalhosEmEvento.sort(function (a, b) { return b - a; });
    for (var i = Math.min(trabalhosEmEvento.length, 3 * contArtigosPublicados) - 1; i >= 0; --i) {
        indices.indiceGeralTrabalhoEmEvento += trabalhosEmEvento[i];
    }

    // Restrição: No máximo 3 conferências a cada 1 periodico são contabilizadas - priorizando as de maior valor
    trabalhosEmEventoRestrito.sort(function (a, b) { return b - a; });
    for (var i = Math.min(trabalhosEmEventoRestrito.length, 3 * contArtigosPublicadosRestrito) - 1; i >= 0; --i) {
        indices.indiceRestritoTrabalhoEmEvento += trabalhosEmEventoRestrito[i];
    }

    indices.indiceGeralArtigoPublicado /= 100;
    indices.indiceGeralTrabalhoEmEvento /= 100;
    indices.indiceGeral = indices.indiceGeralTrabalhoEmEvento + indices.indiceGeralArtigoPublicado;

    indices.indiceRestritoArtigoPublicado /= 100;
    indices.indiceRestritoTrabalhoEmEvento /= 100;
    indices.indiceRestrito = indices.indiceRestritoTrabalhoEmEvento + indices.indiceRestritoArtigoPublicado;

    return indices;
}

function filtroTipoTrabalhoEmEvento(value) {
    return value._fields[0].properties.tipo === "trabalhoEmEvento";
}

function filtroTipoArtigoPublicado(value) {
    return value._fields[0].properties.tipo === "artigoPublicado";
}