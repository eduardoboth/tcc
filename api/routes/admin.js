var shortid = require('shortid');
var neo4j = require('neo4j-driver').v1;
var fs = require('fs');
var Ajv = require('ajv');
var xml2js = require('xml2js');
var stringSimilarity = require('string-similarity');
var moment = require('moment');

var appConfig = JSON.parse(fs.readFileSync('./config/appConfig.json', 'utf8'));
var driver = neo4j.driver(appConfig.neo4jURL, neo4j.auth.basic(appConfig.neo4jUser, appConfig.neo4jPwd));

var qualisPeriodicos = JSON.parse(fs.readFileSync('./config/qualisPeriodicos.json', 'utf8'));
var qualisConferencias = JSON.parse(fs.readFileSync('./config/qualisConferencias.json', 'utf8'));

var schemaAutor = JSON.parse(fs.readFileSync('./schemas/Autor.json', 'utf8'));
var schemaArtigoPublicado = JSON.parse(fs.readFileSync('./schemas/ArtigoPublicado.json', 'utf8'));
var schemaTrabalhoEmEvento = JSON.parse(fs.readFileSync('./schemas/TrabalhoEmEvento.json', 'utf8'));
var schemaGrupo = JSON.parse(fs.readFileSync('./schemas/Grupo.json', 'utf8'));

var ajv = new Ajv();
ajv.addSchema(schemaAutor);
ajv.addSchema(schemaArtigoPublicado);
ajv.addSchema(schemaTrabalhoEmEvento);
ajv.addSchema(schemaGrupo);

var validateAutor = ajv.compile(schemaAutor);
var validateArtigoPublicado = ajv.compile(schemaArtigoPublicado);
var validateTrabalhoEmEvento = ajv.compile(schemaTrabalhoEmEvento);
var validateGrupo = ajv.compile(schemaGrupo);

var admin = {

    createGrupo: function (req, res) {
        var grupo = req.body;

        if (validateGrupo(grupo)) {
            grupo.dados.uuid = shortid.generate();
            var query = "WITH {grupo}.dados AS dadosGrupo " +
                "MERGE (g:Grupo { nome: dadosGrupo.nome }) SET g = dadosGrupo " +
                "WITH g " +
                "UNWIND {grupo}.numeroIdCNPQAutores AS idsAutores " +
                "MATCH (a:Autor) WHERE a.numeroIdCNPQ IN idsAutores " +
                "MERGE (a)-[:MEMBRO]->(g)";

            var session = driver.session();
            var tx = session.beginTransaction();
            tx
                .run(query, { grupo: grupo })
                .then(function (result) {
                    tx.commit();
                    session.close();
                    res.rest.ok();
                })
                .catch(function (error) {
                    tx.rollback();
                    session.close();
                    res.rest.serverError(error.fields[0].message);
                });
        } else {
            res.rest.badRequest(validateGrupo.errors);
        }
    },

    uploadCurriculo: function (req, res) {
        var curriculo = req.body;
        var autor = parseCurriculoXmlToAutor(curriculo);

        if (validateAutor(autor)) {
            try {
                mergeCurriculo(autor);
                res.rest.ok(autor);
            } catch (error) {
                res.rest.serverError(error.fields[0].message);
            }
        } else {
            res.rest.badRequest(validateAutor.errors);
        }
    },

    parseCurriculo: function (req, res) {
        var curriculo = req.body;
        var autor = parseCurriculoXmlToAutor(curriculo);

        res.rest.ok(autor);
    }
};

module.exports = admin;

function mergeCurriculo(curriculo) {
    for (var i = 0; i < curriculo.publicacoes.artigosPublicados.length; ++i) {
        curriculo.publicacoes.artigosPublicados[i].dados.uuid = shortid.generate();
    }

    for (var i = 0; i < curriculo.publicacoes.trabalhosEmEventos.length; ++i) {
        curriculo.publicacoes.trabalhosEmEventos[i].dados.uuid = shortid.generate();
    }

    var queryMergeAutor = "MERGE (a:Autor { numeroIdCNPQ: {dadosAutor}.numeroIdCNPQ }) " +
        " ON CREATE SET a = {dadosAutor}, a.curriculoCompleto = true " +
        " ON MATCH SET a.curriculoCompleto = true, a.ultimaAtualizacao = {dadosAutor}.ultimaAtualizacao";

    var queryMergeArtigosPublicados = "UNWIND {artigosPublicados} as artigo " +
        "MERGE (p:Publicacao { tipo: artigo.dados.tipo, natureza: artigo.dados.natureza, tituloNormalizadoDoArtigo: artigo.dados.tituloNormalizadoDoArtigo, anoDoArtigo: artigo.dados.anoDoArtigo, qualis: artigo.dados.qualis }) " +
        " ON CREATE SET p = artigo.dados " +
        "WITH p, artigo " +
        "UNWIND artigo.autores AS autor " +
        "OPTIONAL MATCH (a:Autor) WHERE a.numeroIdCNPQ = autor.dados.numeroIdCNPQ AND a.numeroIdCNPQ <> '' WITH count(a) as encontrados, autor, p " +
        "FOREACH(ignoreMe IN CASE WHEN encontrados = 0 THEN [1] ELSE [] END | " +
        " MERGE (aa:Autor { nomeCompletoNormalizado: autor.dados.nomeCompletoNormalizado }) " +
        "   ON CREATE SET aa = autor.dados, aa.curriculoCompleto = false " +
        " MERGE (aa)-[:CONTRIBUIU]->(p) " +
        ") " +
        "FOREACH(ignoreMe IN CASE WHEN encontrados > 0 THEN [1] ELSE [] END | " +
        " MERGE (aa:Autor { numeroIdCNPQ: autor.dados.numeroIdCNPQ }) " +
        "   ON CREATE SET aa = autor.dados, aa.curriculoCompleto = false " +
        " MERGE (aa)-[:CONTRIBUIU]->(p) " +
        ") ";

    var queryMergeTrabalhosEmEventos = "UNWIND {trabalhosEmEventos} as trabalho " +
        "MERGE (p:Publicacao { tipo: trabalho.dados.tipo, natureza: trabalho.dados.natureza, tituloNormalizadoDoTrabalho: trabalho.dados.tituloNormalizadoDoTrabalho, anoDoTrabalho: trabalho.dados.anoDoTrabalho, qualis: trabalho.dados.qualis }) " +
        " ON CREATE SET p = trabalho.dados " +
        "WITH p, trabalho " +
        "UNWIND trabalho.autores AS autor " +
        "OPTIONAL MATCH (a:Autor) WHERE a.numeroIdCNPQ = autor.dados.numeroIdCNPQ AND a.numeroIdCNPQ <> '' WITH count(a) as encontrados, autor, p " +
        "FOREACH(ignoreMe IN CASE WHEN encontrados = 0 THEN [1] ELSE [] END | " +
        " MERGE (aa:Autor { nomeCompletoNormalizado: autor.dados.nomeCompletoNormalizado }) " +
        "   ON CREATE SET aa = autor.dados, aa.curriculoCompleto = false " +
        " MERGE (aa)-[:CONTRIBUIU]->(p) " +
        ") " +
        "FOREACH(ignoreMe IN CASE WHEN encontrados > 0 THEN [1] ELSE [] END | " +
        " MERGE (aa:Autor { numeroIdCNPQ: autor.dados.numeroIdCNPQ }) " +
        "   ON CREATE SET aa = autor.dados, aa.curriculoCompleto = false " +
        " MERGE (aa)-[:CONTRIBUIU]->(p) " +
        ") ";


    var session = driver.session();
    var tx = session.beginTransaction();
    tx
        .run(queryMergeAutor, { dadosAutor: curriculo.dados })
        .then(function (result) {
            tx
                .run(queryMergeArtigosPublicados, { artigosPublicados: curriculo.publicacoes.artigosPublicados })
                .then(function (result) {
                    tx
                        .run(queryMergeTrabalhosEmEventos, { trabalhosEmEventos: curriculo.publicacoes.trabalhosEmEventos })
                        .then(function (result) {
                            tx.commit();
                            session.close();
                        })
                        .catch(function (error) {
                            tx.rollback();
                            session.close();
                            throw error;
                        });
                })
                .catch(function (error) {
                    tx.rollback();
                    session.close();
                    throw error;
                });
        })
        .catch(function (error) {
            tx.rollback();
            session.close();
            throw error;
        });
}

function parseCurriculoXmlToAutor(curriculo) {
    var autor = {};
    autor.dados = {};
    autor.publicacoes = {};

    autor.dados.numeroIdCNPQ = curriculo["curriculo-vitae"].$["NUMERO-IDENTIFICADOR"].trim();
    autor.dados.nomeCompleto = curriculo["curriculo-vitae"]["dados-gerais"][0].$["NOME-COMPLETO"].trim();
    autor.dados.nomeCompletoNormalizado = autor.dados.nomeCompleto.toLowerCase();
    autor.dados.ultimaAtualizacao = moment(curriculo["curriculo-vitae"].$["DATA-ATUALIZACAO"] + " " + curriculo["curriculo-vitae"].$["HORA-ATUALIZACAO"], 'DDMMYYYY HHmmss').toDate().toISOString();

    var trabalhosEmEventos = (curriculo["curriculo-vitae"]["producao-bibliografica"][0]["trabalhos-em-eventos"][0]["trabalho-em-eventos"]).filter(filtroTipoTrabalhoEmEvento).map(parseTrabalhoEmEvento);
    autor.publicacoes.trabalhosEmEventos = trabalhosEmEventos;

    var artigosPublicados = (curriculo["curriculo-vitae"]["producao-bibliografica"][0]["artigos-publicados"][0]["artigo-publicado"]).filter(filtroTipoArtigoPublicado).map(parseArtigoPublicado);
    autor.publicacoes.artigosPublicados = artigosPublicados;

    return autor;
}

function filtroTipoTrabalhoEmEvento(value) {
    return value["dados-basicos-do-trabalho"][0].$.NATUREZA.toUpperCase() === "COMPLETO";
}

function filtroTipoArtigoPublicado(value) {
    return value["dados-basicos-do-artigo"][0].$.NATUREZA.toUpperCase() === "COMPLETO";
}

function parseTrabalhoEmEvento(evento) {
    var dados = {};
    dados.tipo = "trabalhoEmEvento";
    dados.natureza = evento["dados-basicos-do-trabalho"][0].$["NATUREZA"].trim().toLowerCase();
    dados.tituloDoTrabalho = evento["dados-basicos-do-trabalho"][0].$["TITULO-DO-TRABALHO"].trim();
    dados.tituloNormalizadoDoTrabalho = dados.tituloDoTrabalho.toLowerCase();
    dados.anoDoTrabalho = evento["dados-basicos-do-trabalho"][0].$["ANO-DO-TRABALHO"].trim();
    dados.paisDoEvento = evento["dados-basicos-do-trabalho"][0].$["PAIS-DO-EVENTO"].trim();
    dados.idioma = evento["dados-basicos-do-trabalho"][0].$["IDIOMA"].trim();
    dados.meioDeDivulgacao = evento["dados-basicos-do-trabalho"][0].$["MEIO-DE-DIVULGACAO"].trim().toLowerCase();
    dados.homePageDoTrabalho = evento["dados-basicos-do-trabalho"][0].$["HOME-PAGE-DO-TRABALHO"].trim();
    dados.flagDeRelevancia = evento["dados-basicos-do-trabalho"][0].$["FLAG-RELEVANCIA"].trim().toLowerCase();
    dados.DOI = evento["dados-basicos-do-trabalho"][0].$["DOI"].trim();
    dados.tituloDoTrabalhoEmIngles = evento["dados-basicos-do-trabalho"][0].$["TITULO-DO-TRABALHO-INGLES"].trim();
    dados.flagDivulgacaoCientifica = evento["dados-basicos-do-trabalho"][0].$["FLAG-DIVULGACAO-CIENTIFICA"].trim().toLowerCase();

    dados.classificacaoDoEvento = evento["detalhamento-do-trabalho"][0].$["CLASSIFICACAO-DO-EVENTO"].trim().toLowerCase();
    dados.nomeDoEvento = evento["detalhamento-do-trabalho"][0].$["NOME-DO-EVENTO"].trim();
    dados.cidadeDoEvento = evento["detalhamento-do-trabalho"][0].$["CIDADE-DO-EVENTO"].trim();
    dados.anoDeRealizacao = evento["detalhamento-do-trabalho"][0].$["ANO-DE-REALIZACAO"].trim();
    dados.tituloDosAnaisOuProceedings = evento["detalhamento-do-trabalho"][0].$["TITULO-DOS-ANAIS-OU-PROCEEDINGS"].trim();
    dados.volume = evento["detalhamento-do-trabalho"][0].$["VOLUME"].trim();
    dados.fasciculo = evento["detalhamento-do-trabalho"][0].$["FASCICULO"].trim();
    dados.serie = evento["detalhamento-do-trabalho"][0].$["SERIE"].trim();
    dados.paginaInicial = evento["detalhamento-do-trabalho"][0].$["PAGINA-INICIAL"].trim();
    dados.paginaFinal = evento["detalhamento-do-trabalho"][0].$["PAGINA-FINAL"].trim();
    dados.ISBN = evento["detalhamento-do-trabalho"][0].$["ISBN"].trim();
    dados.nomeDaEditora = evento["detalhamento-do-trabalho"][0].$["NOME-DA-EDITORA"].trim();
    dados.cidadeDaEditora = evento["detalhamento-do-trabalho"][0].$["CIDADE-DA-EDITORA"].trim();
    dados.nomeDoEventoEmIngles = evento["detalhamento-do-trabalho"][0].$["NOME-DO-EVENTO-INGLES"].trim();

    var qualis = buscaQualisConferencia(dados.anoDeRealizacao, dados.nomeDoEvento);
    dados.qualis = qualis.classificacao;
    dados.anoDoQualis = qualis.anoDaClassificacao;

    var autores = evento["autores"].map(mapAutorToAutor);

    var trabalhoEmEvento = {};
    trabalhoEmEvento.dados = dados;
    trabalhoEmEvento.autores = autores;

    return trabalhoEmEvento;
}

function parseArtigoPublicado(artigo) {
    var dados = {};
    dados.tipo = "artigoPublicado";
    dados.natureza = artigo["dados-basicos-do-artigo"][0].$["NATUREZA"].trim().toLowerCase();
    dados.tituloDoArtigo = artigo["dados-basicos-do-artigo"][0].$["TITULO-DO-ARTIGO"].trim();
    dados.tituloNormalizadoDoArtigo = dados.tituloDoArtigo.toLowerCase();
    dados.anoDoArtigo = artigo["dados-basicos-do-artigo"][0].$["ANO-DO-ARTIGO"].trim();
    dados.paisDePublicacao = artigo["dados-basicos-do-artigo"][0].$["PAIS-DE-PUBLICACAO"].trim();
    dados.idioma = artigo["dados-basicos-do-artigo"][0].$["IDIOMA"].trim();
    dados.meioDeDivulgacao = artigo["dados-basicos-do-artigo"][0].$["MEIO-DE-DIVULGACAO"].trim().toLowerCase();
    dados.homePageDoTrabalho = artigo["dados-basicos-do-artigo"][0].$["HOME-PAGE-DO-TRABALHO"].trim();
    dados.flagDeRelevancia = artigo["dados-basicos-do-artigo"][0].$["FLAG-RELEVANCIA"].trim().toLowerCase();
    dados.DOI = artigo["dados-basicos-do-artigo"][0].$["DOI"].trim();
    dados.tituloDoArtigoEmIngles = artigo["dados-basicos-do-artigo"][0].$["TITULO-DO-ARTIGO-INGLES"].trim();
    dados.flagDivulgacaoCientifica = artigo["dados-basicos-do-artigo"][0].$["FLAG-DIVULGACAO-CIENTIFICA"].trim().toLowerCase();

    dados.tituloDoPeriodicoOuRevista = artigo["detalhamento-do-artigo"][0].$["TITULO-DO-PERIODICO-OU-REVISTA"].trim();
    dados.ISSN = artigo["detalhamento-do-artigo"][0].$["ISSN"].trim();
    dados.volume = artigo["detalhamento-do-artigo"][0].$["VOLUME"].trim();
    dados.fasciculo = artigo["detalhamento-do-artigo"][0].$["FASCICULO"].trim();
    dados.serie = artigo["detalhamento-do-artigo"][0].$["SERIE"].trim();
    dados.paginaInicial = artigo["detalhamento-do-artigo"][0].$["PAGINA-INICIAL"].trim();
    dados.paginaFinal = artigo["detalhamento-do-artigo"][0].$["PAGINA-FINAL"].trim();
    dados.localDePublicacao = artigo["detalhamento-do-artigo"][0].$["LOCAL-DE-PUBLICACAO"].trim();

    var qualis = buscaQualisArtigo(dados.anoDoArtigo, dados.ISSN);
    dados.qualis = qualis.classificacao;
    dados.anoDoQualis = qualis.anoDaClassificacao;

    var autores = artigo["autores"].map(mapAutorToAutor);

    var artigoPublicado = {};
    artigoPublicado.dados = dados;
    artigoPublicado.autores = autores;

    return artigoPublicado;
}

function mapAutorToAutor(autorXML) {
    var autor = {};
    autor.dados = {};
    autor.dados.numeroIdCNPQ = autorXML.$["NRO-ID-CNPQ"].trim();
    autor.dados.nomeCompleto = autorXML.$["NOME-COMPLETO-DO-AUTOR"].trim();
    autor.dados.nomeCompletoNormalizado = autor.dados.nomeCompleto.toLowerCase();
    autor.dados.nomeParaCitacao = autorXML.$["NOME-PARA-CITACAO"].trim();
    autor.dados.ultimaAtualizacao = "";

    return autor;
}

/*
 * Heurística de busca do qualis:
 * - Busca qualis no ano do artigo
 * - Se não encontrar, busca nos anos anteriores e retorna o mais recente encontrado
 * - Se não encontrar, busca nos anos posteriores e retorna o primeiro encontrado
 * - Se não encontrar, qualis fica como "NA" (não atribuido)
 */
function buscaQualisArtigo(anoDoArtigo, issn) {
    var qualis = {};

    var ano = parseInt(anoDoArtigo, 10);

    if (ano < qualisPeriodicos.primeiroAnoDisponivel) {
        ano = qualisPeriodicos.primeiroAnoDisponivel;
    } else if (ano > qualisPeriodicos.ultimoAnoDisponivel) {
        ano = qualisPeriodicos.ultimoAnoDisponivel;
    }

    for (var anoAtualNum = ano; anoAtualNum >= qualisPeriodicos.primeiroAnoDisponivel; --anoAtualNum) {
        var anoAtual = anoAtualNum.toString();

        if (qualisPeriodicos[anoAtual] != null && qualisPeriodicos[anoAtual][issn] != null) {
            qualis.classificacao = qualisPeriodicos[anoAtual][issn].classificacao;
            qualis.anoDaClassificacao = anoAtual;
            return qualis;
        }
    }

    for (var anoAtualNum = ano; anoAtualNum <= qualisPeriodicos.ultimoAnoDisponivel; ++anoAtualNum) {
        var anoAtual = anoAtualNum.toString();

        if (qualisPeriodicos[anoAtual] != null && qualisPeriodicos[anoAtual][issn] != null) {
            qualis.classificacao = qualisPeriodicos[anoAtual][issn].classificacao;
            qualis.anoDaClassificacao = anoAtual;
            return qualis;
        }
    }

    qualis.classificacao = "NA";
    qualis.anoDaClassificacao = "NA";
    return qualis;
}

/*
 * Heurística de busca do qualis:
 * - Busca qualis no ano da conferencia
 * - Se não encontrar, busca nos anos anteriores e retorna o mais recente encontrado
 * - Se não encontrar, busca nos anos posteriores e retorna o primeiro encontrado
 * - Se não encontrar, qualis fica como "NA" (não atribuido)
 */
function buscaQualisConferencia(anoDaConferencia, nomeDaConferencia) {
    var qualis = {};

    var ano = parseInt(anoDaConferencia, 10);

    if (ano < qualisConferencias.primeiroAnoDisponivel) {
        ano = qualisConferencias.primeiroAnoDisponivel;
    } else if (ano > qualisConferencias.ultimoAnoDisponivel) {
        ano = qualisConferencias.ultimoAnoDisponivel;
    }

    for (var anoAtualNum = ano; anoAtualNum >= qualisConferencias.primeiroAnoDisponivel; --anoAtualNum) {
        var anoAtual = anoAtualNum.toString();

        if (qualisConferencias[anoAtual] != null) {
            var classificacao = buscaQualisConferenciaPorNome(nomeDaConferencia, qualisConferencias[anoAtual]);
            if (classificacao != null) {
                qualis.classificacao = classificacao;
                qualis.anoDaClassificacao = anoAtual;
                return qualis;
            }
        }
    }

    for (var anoAtualNum = ano; anoAtualNum <= qualisConferencias.ultimoAnoDisponivel; ++anoAtualNum) {
        var anoAtual = anoAtualNum.toString();

        if (qualisConferencias[anoAtual] != null) {
            var classificacao = buscaQualisConferenciaPorNome(nomeDaConferencia, qualisConferencias[anoAtual]);
            if (classificacao != null) {
                qualis.classificacao = classificacao;
                qualis.anoDaClassificacao = anoAtual;
                return qualis;
            }
        }
    }

    qualis.classificacao = "NA";
    qualis.anoDaClassificacao = "NA";
    return qualis;
}

function buscaQualisConferenciaPorNome(nomeConferencia, listaConferencias) {
    for (var i = 0; i < listaConferencias.length; ++i) {
        var similaridade = stringSimilarity.compareTwoStrings(listaConferencias[i].nome.toUpperCase(), nomeConferencia.toUpperCase());
        if (similaridade >= 0.7) {
            return listaConferencias[i].classificacao;
        }
    }

    return null;
}