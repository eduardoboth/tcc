angular.module('GrupoCtrl', []).controller('GrupoController', function ($scope, $routeParams, Grupos, Autores) {

    Grupos.get($routeParams.id)
        .then(function (response) {
            $scope.grupo = response.data;
            console.log(status);
            Grupos.getIndices($routeParams.id)
                .then(function (response) {
                    $scope.grupo.indices = response.data;

                    $scope.membrosDetalhados = [];
                    $scope.grupo.membros.forEach(function (idAutor) {
                        Autores.get(idAutor).then(function (response) {
                            $scope.membrosDetalhados.push(response.data);
                        }, function (response) {
                            console.log(response.status);
                            console.log("Error!");
                        });
                    });

                }, function (response) {
                    console.log(response.status);
                    console.log("Error!");
                });
        }, function (response) {
            console.log(response.status);
            console.log("Error!");
        });

});