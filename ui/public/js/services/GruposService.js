angular.module('GruposService', []).factory('Grupos', function ($http) {

    return {

        list : function() {
            return $http.get('http://localhost:8050/api/v1/grupos');
        },

        get : function(id) {
            return $http.get('http://localhost:8050/api/v1/grupos/' + id);
        },

        getIndices : function(id) {
            return $http.get('http://localhost:8050/api/v1/indices/' + id);
        }
    }       

});