angular.module('AutoresService', []).factory('Autores', function($http) {

    return {

        list : function() {
            return $http.get('http://localhost:8050/api/v1/autores');
        },

        get : function(id) {
            return $http.get('http://localhost:8050/api/v1/autores/' + id);
        }
    }       

});