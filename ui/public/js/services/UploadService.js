angular.module('UploadService', []).factory('Upload', function ($http) {

    return {

        uploadCurriculo: function (curriculo) {
            return $http({
                method: 'POST',
                url: 'http://localhost:8050/api/v1/admin/uploadCurriculo',
                data: curriculo,
                headers: { 'Content-Type': 'application/xml' }
            });
        }
    }

});