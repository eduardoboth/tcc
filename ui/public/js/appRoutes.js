angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {

    $routeProvider

        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'MainController'
        })

        .when('/autores', {
            templateUrl: 'views/autores.html',
            controller: 'AutoresController'
        })
        
        .when('/autores/:id', {
            templateUrl: 'views/autor.html',
            controller: 'AutorController'
        })

        .when('/grupos', {
            templateUrl: 'views/grupos.html',
            controller: 'GruposController'
        })

        .when('/grupos/:id', {
            templateUrl: 'views/grupo.html',
            controller: 'GrupoController'
        })

        .when('/upload', {
            templateUrl: 'views/upload.html',
            controller: 'UploadController'
        });

    $locationProvider.html5Mode(true);

}]);
